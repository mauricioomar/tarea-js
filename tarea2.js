// Declarar el vector con 6 elementos de diferentes tipos de datos
var vector = [1, "dos", true, null, undefined, { nombre: "elemento" }];

//Imprimir en la consola cada valor usando "for"
console.log("Imprimir usando for:");
for (var i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

//Imprimir en la consola cada valor usando "forEach"
console.log("Imprimir usando forEach:");
vector.forEach(function(elemento) {
  console.log(elemento);
});

//Imprimir en la consola cada valor usando "map"
console.log("Imprimir usando map:");
vector.map(function(elemento) {
  console.log(elemento);
});

//Imprimir en la consola cada valor usando "while"
console.log("Imprimir usando while:");
var i = 0;
while (i < vector.length) {
  console.log(vector[i]);
  i++;
}

//Imprimir en la consola cada valor usando "for..of"
console.log("Imprimir usando for..of:");
for (var elemento of vector) {
  console.log(elemento);
}
