// Declarar el vector con 6 elementos de diferentes tipos de datos
var vector = [1, "dos", true, null, undefined, { nombre: "elemento" }];

// Imprimir en la consola el vector
console.log(vector);

// Imprimir en la consola el primer y último elemento del vector usando sus índices
console.log("Primer elemento:", vector[0]);
console.log("Último elemento:", vector[vector.length - 1]);

// Modificar el valor del tercer elemento
vector[2] = false;

// Imprimir en la consola la longitud del vector
console.log("Longitud del vector:", vector.length);

// Agregar un elemento al vector usando "push"
vector.push(7);

// Eliminar el último elemento e imprimirlo usando "pop"
var ultimoElemento = vector.pop();
console.log("Último elemento eliminado:", ultimoElemento);

// Agregar un elemento en la mitad del vector usando "splice"
vector.splice(3, 0, "nuevoElemento");

// Eliminar el primer elemento usando "shift"
var primerElemento = vector.shift();
console.log("Primer elemento eliminado:", primerElemento);

// Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primerElemento);

// Imprimir en la consola el vector actualizado
console.log("Vector actualizado:", vector);
